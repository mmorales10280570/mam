package entidad;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.Basic;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
/**
 *
 * @author Martha
 */
@Entity
@Table(name = "producto")
@NamedQueries({
    @NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p")})
public class Producto implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idProducto")
    private Integer idProducto;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "precio")
    private Float precio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private Collection<Lineaorden> lineaordenCollection;

    public Producto() {}

    public Producto(Integer idProducto) 
    {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() 
    {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto)
    {
        this.idProducto = idProducto;
    }

    public String getDescripcion() 
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Float getPrecio() 
    {
        return precio;
    }

    public void setPrecio(Float precio) 
    {
        this.precio = precio;
    }

    public Collection<Lineaorden> getLineaordenCollection() 
    {
        return lineaordenCollection;
    }

    public void setLineaordenCollection(Collection<Lineaorden> lineaordenCollection)
    {
        this.lineaordenCollection = lineaordenCollection;
    }

    @Override
    public int hashCode() 
    {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) 
    {
        if (!(object instanceof Producto)) 
        {
            return false;
        }
        Producto other = (Producto) object;
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "entidad.Producto[ idProducto=" + idProducto + " ]";
    }
    
}
