package entidad;
/**
 *
 * @author Martha
 */
import javax.persistence.*;
import entidad.*;
import java.util.Date;
public class Principal 
{
    public static void main(String args[])
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Linea-OrdenPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Date fecha = new Date();
        
        Producto producto1 = new Producto();
        producto1.setIdProducto(1);
        producto1.setDescripcion("Blusa azul 234");
        producto1.setPrecio(210f);
        
        Producto producto2 = new Producto();
        producto2.setIdProducto(5);
        producto2.setDescripcion("Sudadera caballero negra 253");
        producto2.setPrecio(340f);
        
        Producto producto3 = new Producto();
        producto3.setIdProducto(9);
        producto3.setDescripcion("Pantalo mezclilla 432");
        producto3.setPrecio(450f);
       
        
        Orden orden1= new Orden();
        orden1.setIdOrden(1);
        orden1.setFechaOrden(fecha);
        
        Orden orden2= new Orden();
        orden2.setIdOrden(5);
        orden2.setFechaOrden(fecha);
        
        Orden orden3= new Orden();
        orden3.setIdOrden(9);
        orden3.setFechaOrden(fecha);
        
        Lineaorden lineaOrden1= new Lineaorden(1,1);
        lineaOrden1.setCantidad(1);       
        
        Lineaorden lineaOrden2= new Lineaorden(1,3);
        lineaOrden2.setCantidad(2);
        
        Lineaorden lineaOrden3= new Lineaorden(2,3);
        lineaOrden3.setCantidad(1);
        
        Lineaorden lineaOrden4= new Lineaorden(2,9);
        lineaOrden4.setCantidad(1);
        
        try 
        {
            em.persist(producto1);
            em.persist(producto2);
            em.persist(producto3);
            
            em.persist(orden1);
            em.persist(orden2);
            em.persist(orden3);
            
            em.persist(lineaOrden1);
            em.persist(lineaOrden2);
            em.persist(lineaOrden3);
            em.persist(lineaOrden4);
           
            
        } catch (Exception e)
        {
            System.out.println("Fallo en el registro de dato" + e.getMessage()); 

        }

        int idProduct1 = producto1.getIdProducto();
        int idProducto2 = producto2.getIdProducto();
        int idProducto3 = producto3.getIdProducto();
        int ordn1Id = orden1.getIdOrden();
        int ordn2Id = orden2.getIdOrden();
        
        System.out.println("PRODUCTOS");
        Producto productoB1 = em.find(Producto.class, idProduct1);
        System.out.println("Producto 1:");
        System.out.println("\t Id: " + productoB1.getIdProducto());
        System.out.println("\t Precio: " + productoB1.getPrecio());
        System.out.println("\t Descripción: " + productoB1.getDescripcion());
        
        
        Producto productoB2 = em.find(Producto.class, idProducto2);
        System.out.println("Producto 2:");
        System.out.println("\t Id: " + productoB2.getIdProducto());
        System.out.println("\t Precio: " + productoB2.getPrecio());
        System.out.println("\t Descripción: " + productoB2.getDescripcion());
        
        
        Producto productoB3 = em.find(Producto.class, idProducto3);
        System.out.println("Producto 3:");
        System.out.println("\t Id: " + productoB3.getIdProducto());
        System.out.println("\t Precio: " + productoB3.getPrecio());
        System.out.println("\t Descripción: " + productoB3.getDescripcion());
        
        System.out.println("ORDENES ");     

        Orden dborden1 = em.find(Orden.class, ordn1Id);
        System.out.println("Orden 1");
        System.out.println("\t Id: " + dborden1.getIdOrden());
        System.out.println("\t Fecha: " + dborden1.getFechaOrden());
        
        Orden dborden2 = em.find(Orden.class, ordn2Id);
        System.out.println("Orden 2");
        System.out.println("\t Id: " + dborden2.getIdOrden());
        System.out.println("\t Fecha: " + dborden2.getFechaOrden());
        
        System.out.println("LINEA DE ORDEN");
        
        LineaordenPK ordenLinea1= lineaOrden1.getLineaordenPK();
        Lineaorden ordenLineaB1=em.find(Lineaorden.class, ordenLinea1);
        System.out.println("Linea De Orden 1");
        System.out.println("\t PK" + ordenLineaB1.getLineaordenPK());        
        System.out.println("\t Orden" + ordenLineaB1.getOrden());
        System.out.println("\t Producto" + ordenLineaB1.getProducto());
        System.out.println("\t Cantidad: " + ordenLineaB1.getCantidad());
        
        LineaordenPK ordenLinea2= lineaOrden2.getLineaordenPK();
        Lineaorden ordenLineaB2=em.find(Lineaorden.class, ordenLinea2);
        System.out.println("Linea De Orden 2");
        System.out.println("\t PK" + ordenLineaB2.getLineaordenPK());        
        System.out.println("\t Orden" + ordenLineaB2.getOrden());
        System.out.println("\t Producto" + ordenLineaB2.getProducto());
        System.out.println("\t Cantidad: " + ordenLineaB2.getCantidad());
        
        LineaordenPK ordenLinea3= lineaOrden3.getLineaordenPK();
        Lineaorden ordenLineaB3=em.find(Lineaorden.class, ordenLinea3);
        System.out.println("Linea De Orden 3");
        System.out.println("\t PK" + ordenLineaB3.getLineaordenPK());        
        System.out.println("\t Orden" + ordenLineaB3.getOrden());
        System.out.println("\t Producto" + ordenLineaB3.getProducto());
        System.out.println("\t Cantidad: " + ordenLineaB3.getCantidad());
        
        LineaordenPK ordenLinea4= lineaOrden4.getLineaordenPK();
        Lineaorden ordenLineaB4=em.find(Lineaorden.class, ordenLinea4);
        System.out.println("Linea De Orden 4");
        System.out.println("\t PK" + ordenLineaB4.getLineaordenPK());        
        System.out.println("\t Orden" + ordenLineaB4.getOrden());
        System.out.println("\t Producto" + ordenLineaB4.getProducto());
        System.out.println("\t Cantidad: " + ordenLineaB4.getCantidad());
        
        try 
        {
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Fallo en la transición"+ e.getMessage());
        }
        em.close();
        emf.close();
    }
}
